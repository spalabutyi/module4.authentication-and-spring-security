package com.epam.esm.application.handler;

import com.epam.esm.exception.response.ErrorResponse;
import com.epam.esm.exception.response.Errors;
import com.epam.esm.repository.exception.OrderNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@PropertySource("classpath:errormessages.properties")
@RequiredArgsConstructor
public class OrderControllerExceptionHandler {

    private final Environment env;

    /*
     * Order exceptions
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleOrderNotFoundException(OrderNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("order.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

}