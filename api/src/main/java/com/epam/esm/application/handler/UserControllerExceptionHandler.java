package com.epam.esm.application.handler;

import com.epam.esm.exception.response.ErrorResponse;
import com.epam.esm.exception.response.Errors;
import com.epam.esm.repository.exception.UserFailCreateException;
import com.epam.esm.repository.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@PropertySource("classpath:errormessages.properties")
@RequiredArgsConstructor
public class UserControllerExceptionHandler {

    private final Environment env;

    /*
     * User exceptions
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleUserNotFoundException(UserNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("user.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleUserFailCreateException(UserFailCreateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("user.creation.failure"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

}