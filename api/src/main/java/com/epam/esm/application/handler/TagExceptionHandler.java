package com.epam.esm.application.handler;

import com.epam.esm.exception.response.ErrorResponse;
import com.epam.esm.exception.response.Errors;
import com.epam.esm.repository.exception.NameExistsException;
import com.epam.esm.repository.exception.TagFailCreateException;
import com.epam.esm.repository.exception.TagNotFoundException;
import com.epam.esm.repository.exception.TagUpdateException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@PropertySource("classpath:errormessages.properties")
@RequiredArgsConstructor
public class TagExceptionHandler {

    private final Environment env;

    /*
     * Tag exceptions
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleTagNotFoundException(TagNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("tag.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleTagFailCreateException(TagFailCreateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("tag.creation.failure"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleTagUpdateException(TagUpdateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("tag.update.not.allowed"),
                        Errors.FORBID_UPDATE), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleNameExistsException(NameExistsException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("name.exists.error"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

}