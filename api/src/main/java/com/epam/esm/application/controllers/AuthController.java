package com.epam.esm.application.controllers;

import com.epam.esm.application.handler.ControllerExceptionHandler;
import com.epam.esm.auth.AuthRequest;
import com.epam.esm.auth.AuthResponse;
import com.epam.esm.auth.AuthenticationService;
import com.epam.esm.auth.RegisterRequest;
import com.epam.esm.repository.exception.UserFailCreateException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RestControllerAdvice(basePackages = "com.epam.esm.application.handler")
@RequestMapping(value = "/api/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class AuthController {

    private final AuthenticationService authService;

    @PostMapping("/registration")
    @PreAuthorize("permitAll()")
    public ResponseEntity<AuthResponse> registration(@RequestBody @Validated RegisterRequest user,
                                                     BindingResult result) {
        if (result.hasErrors())
            throw new UserFailCreateException(
                    ControllerExceptionHandler.getErrorMessages(result));

        return ResponseEntity.ok(authService.register(user));
    }

    @PostMapping("/login")
    @PreAuthorize("permitAll()")
    public ResponseEntity<AuthResponse> login(@RequestBody AuthRequest user) {
        return ResponseEntity.ok(authService.authenticate(user));
    }

    @GetMapping("token/refresh")
    @PreAuthorize("permitAll()")
    public ResponseEntity<AuthResponse> refreshToken(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
        String refreshToken = headerAuth.substring(7);

        return ResponseEntity.ok(authService.parseRefreshToken(refreshToken));
    }

}
