package com.epam.esm.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class RegisterRequest {

    @NotBlank
    @Size(min = 2, max = 32, message = "Name must be between 2 and 32 characters")
    private String name;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(min = 8, max = 32, message = "Must be between 8 and 32 char")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

}