package com.epam.esm.security;

import com.epam.esm.exception.TokenException;
import com.epam.esm.model.User;
import com.nimbusds.jwt.SignedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtUtil {

    private static final String ISSUER = "com.epam.esm";
    public static final String ERROR_MSG = "Cannot parse token: ";
    @Value("${security.jwt.expiration}")
    private long jwtExpiration;

    @Value("${security.jwt.refresh-token.expiration}")
    private long refreshExpiration;


    private final JwtEncoder encoder;

    public String getToken(User user) {
        JwtClaimsSet claims = generateToken(user, jwtExpiration);
        return this.encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }

    public String getRefreshToken(User user) {
        JwtClaimsSet claims = generateToken(user, refreshExpiration);
        return this.encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }

    private static JwtClaimsSet generateToken(User user, long expiry) {
        Instant now = Instant.now();
        String scope = user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(" "));
        return JwtClaimsSet.builder()
                .issuer(ISSUER)
                .issuedAt(now)
                .expiresAt(now.plusSeconds(expiry))
                .subject(user.getUsername())
                .claim("scope", scope)
                .build();
    }

    public String extractUsername(String token) {
        try {
            SignedJWT decodedJWT = SignedJWT.parse(token);
            return decodedJWT.getJWTClaimsSet().getSubject();
        } catch (ParseException e) {
            throw new TokenException(ERROR_MSG + e.getMessage());
        }
    }

    public Date extractExpiration(String token) {
        try {
            SignedJWT decodedJWT = SignedJWT.parse(token);
            return decodedJWT.getJWTClaimsSet().getExpirationTime();
        } catch (ParseException e) {
            throw new TokenException(ERROR_MSG + e.getMessage());
        }
    }

    public Date extractIssue(String token) {
        try {
            SignedJWT decodedJWT = SignedJWT.parse(token);
            return decodedJWT.getJWTClaimsSet().getIssueTime();
        } catch (ParseException e) {
            throw new TokenException(ERROR_MSG + e.getMessage());
        }
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

}
