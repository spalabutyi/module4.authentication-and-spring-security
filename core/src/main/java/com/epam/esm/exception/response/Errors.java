package com.epam.esm.exception.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Errors {

    FAIL_CREATE(44400),
    BAD_REQUEST(40400),
    UNAUTHORISED(40401),
    FORBID_UPDATE(40403),
    ACCESS_DENIED(404033),
    NOT_FOUND(40404),
    NOT_ALLOWED(40405);

    private final int errorCode;

}