package com.epam.esm.exception;

public class TokenException extends RuntimeException{
    public TokenException(String message) {
        super(message);
    }
}
