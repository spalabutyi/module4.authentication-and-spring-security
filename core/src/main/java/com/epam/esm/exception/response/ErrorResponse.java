package com.epam.esm.exception.response;

import lombok.Builder;
import lombok.Data;

@Data
public class ErrorResponse {

    private final String errorMessage;
    private final long errorCode;

    public ErrorResponse(String errorMessage, Errors errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode.getErrorCode();
    }
}
