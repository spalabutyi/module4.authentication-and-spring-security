package com.epam.esm.service.impl;

import com.epam.esm.model.Certificate;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.exception.CertificateAddTagException;
import com.epam.esm.repository.exception.CertificateNotFoundException;
import com.epam.esm.repository.exception.TagFailCreateException;
import com.epam.esm.repository.exception.TagNotFoundException;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.dto.CertificateUpdateDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class CertificateServiceImpl implements CertificateService {

    public static final String ON_DELETE_MESSAGE = "This certificate does not include this tag";
    public static final String ON_ADD_MESSAGE = "This certificate already includes this tag";
    private final CertificateRepository certificateRepository;
    private final TagRepository tagRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public CertificateDTO add(CertificateDTO certificateDTO) {
        if (isExists(certificateDTO.getName())) {
            throw new TagFailCreateException("The name '" + certificateDTO.getName() + "' is already in use. Please choose a different name");
        }
        Certificate certificate = modelMapper.map(certificateDTO, Certificate.class);

        certificate = certificate.toBuilder()
                .createDate(LocalDateTime.now())
                .lastUpdateDate(LocalDateTime.now())
                .build();

//        certificate.setId(0L);
//        certificate.setCreateDate(LocalDateTime.now());
//        certificate.setLastUpdateDate(LocalDateTime.now());

        Certificate saved = certificateRepository.save(certificate);

        return modelMapper.map(saved, CertificateDTO.class);
    }

    @Override
    @Transactional
    public CertificateDTO update(CertificateDTO certificateDTO) {

        Certificate saved = certificateRepository
                .save(modelMapper.map(certificateDTO, Certificate.class));

        return modelMapper.map(saved, CertificateDTO.class);
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        try {
            certificateRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public CertificateDTO get(Long id) {
        Optional<Certificate> certificateOptional = certificateRepository.findById(id);

        if (certificateOptional.isEmpty())
            throw new CertificateNotFoundException();

        return modelMapper.map(certificateOptional, CertificateDTO.class);
    }

    @Override
    public List<CertificateDTO> getAll() {
        return certificateRepository.findAll().stream()
                .map(c -> modelMapper.map(c, CertificateDTO.class))
                .toList();
    }

    @Override
    public boolean isExists(Long id) {
        return certificateRepository.existsById(id);
    }

    @Override
    public boolean isExists(String name) {
        return certificateRepository.findByName(name).isPresent();
    }


    @Override
    @Transactional
    public CertificateDTO update(Long id, CertificateUpdateDTO certificateUpdateDTO) {

        String name = certificateUpdateDTO.getName();
        String description = certificateUpdateDTO.getDescription();
        Integer duration = certificateUpdateDTO.getDuration();
        BigDecimal price = certificateUpdateDTO.getPrice();

        CertificateDTO dto = get(id);
        if (dto != null) {
            if (name != null) dto.setName(name);
            if (description != null) dto.setDescription(description);
            if (duration != null) dto.setDuration(duration);
            if (price != null) dto.setPrice(price);
            dto.setLastUpdateDate(LocalDateTime.now());
        } else {
            throw new CertificateNotFoundException("Wrong certificate id " + id);
        }
        return update(dto);
    }

    @Override
    public boolean hasTags(Long certificateId, Long tagId) {
        boolean isPresent = false;
        Optional<Certificate> certificateOptional = certificateRepository.findById(certificateId);
        if (certificateOptional.isPresent()) {
            Set<Tag> tags = certificateOptional.get().getTags();
            for (Tag t : tags
            ) {
                if (t.getId().equals(tagId)) {
                    isPresent = true;
                    break;
                }
            }
        } else {
            throw new CertificateNotFoundException();
        }
        return isPresent;
    }

    @Override
    @Transactional
    public boolean addTag(Long certificateId, Long tagId) {
        boolean added = false;
        if (!certificateRepository.existsById(certificateId)) {
            throw new CertificateNotFoundException();
        }
        if (!tagRepository.existsById(tagId)) {
            throw new TagNotFoundException();
        }
        if (hasTags(certificateId, tagId)) {
            throw new CertificateAddTagException(ON_ADD_MESSAGE);
        }
        Optional<Tag> tagOptional = tagRepository.findById(tagId);
        Optional<Certificate> certificate = certificateRepository.findById(certificateId);

        if (certificate.isPresent() && tagOptional.isPresent()) {
            certificate.get().getTags().add(tagOptional.get());
            added = true;
        }
        return added;
    }

    @Override
    @Transactional
    public boolean deleteTag(Long certificateId, Long tagId) {
        boolean deleted = false;
        if (!certificateRepository.existsById(certificateId)) {
            throw new CertificateNotFoundException();
        }
        if (!tagRepository.existsById(tagId)) {
            throw new TagNotFoundException();
        }
        if (!hasTags(certificateId, tagId)) {
            throw new CertificateAddTagException(ON_DELETE_MESSAGE);
        }
        Optional<Tag> tag = tagRepository.findById(tagId);
        Optional<Certificate> certificate = certificateRepository.findById(certificateId);

        if (certificate.isPresent() && tag.isPresent()) {
            certificate.get().getTags().remove(tag.get());
            deleted = true;
        }
        return deleted;
    }


    @Override
    public List<CertificateDTO> findWithPagination(Integer page, Integer size,
                                                   String name, String description, String tagName,
                                                   String sortBy, String sortOrder) {

        Sort.Direction direction = sortOrder.equalsIgnoreCase("asc") ?
                Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageable = PageRequest.of(page, size, Sort.by(direction, sortBy));

        Page<Certificate> list;

        if (name != null && description == null && tagName == null) {
            list = certificateRepository.findByNameContaining(name, pageable);
        } else if (name != null && description != null && tagName == null) {
            list = certificateRepository.findByNameContainingAndDescriptionContaining(name, description, pageable);
        } else if (name == null && description != null && tagName == null) {
            list = certificateRepository.findByDescriptionContaining(description, pageable);
        } else if (name == null && description == null && tagName != null) {
            list = certificateRepository.findByTagName(tagName, pageable);
        } else {
            list = certificateRepository.findAll(pageable);
        }

        return list.getContent().stream()
                .map(c -> modelMapper.map(c, CertificateDTO.class))
                .toList();
    }

    @Override
    public List<CertificateDTO> findByTwoTags(String firstTagName, String secondTagName) {
        List<Certificate> list = certificateRepository.findByTags(firstTagName, secondTagName);
        return list.stream()
                .map(certificate -> modelMapper.map(certificate, CertificateDTO.class))
                .toList();
    }
}