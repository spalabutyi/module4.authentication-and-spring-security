package com.epam.esm.service.impl;

import com.epam.esm.model.Certificate;
import com.epam.esm.model.Order;
import com.epam.esm.model.User;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.repository.exception.CertificateNotFoundException;
import com.epam.esm.repository.exception.OrderNotFoundException;
import com.epam.esm.repository.exception.UserNotFoundException;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.dto.OrderDTO;
import com.epam.esm.exception.ServiceException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class OrderServiceImpl implements OrderService {

    public static final String ORDER_EXISTS = "The user has already placed such an order.";
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final CertificateRepository certificateRepository;
    private final ModelMapper modelMapper;

    @Override
    public OrderDTO add(OrderDTO orderDTO) {
        return null;
    }

    @Override
    public OrderDTO update(OrderDTO orderDTO) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public OrderDTO get(Long id) {
        Optional<Order> order = orderRepository.findById(id);

        if (order.isEmpty())
            throw new OrderNotFoundException();

        return modelMapper.map(order, OrderDTO.class);
    }

    @Override
    public List<OrderDTO> getAll() {
        return orderRepository.findAll().stream()
                .map(o -> modelMapper.map(o, OrderDTO.class))
                .toList();
    }

    @Override
    public boolean isExists(Long id) {
        return orderRepository.existsById(id);
    }

    @Override
    public boolean isExists(String name) {
        return false;
    }

    @Override
    public List<OrderDTO> findWithPagination(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Order> orders = orderRepository.findAll(pageable);

        return orders.getContent().stream()
                .map(order -> modelMapper.map(order, OrderDTO.class))
                .toList();
    }

    @Override
    public List<OrderDTO> findUsersOrders(Long userId) {
        return orderRepository.findOrdersByUserId(userId).stream()
                .map(order -> modelMapper.map(order, OrderDTO.class))
                .toList();
    }

    @Override
    public OrderDTO findByIdAndUserId(Long userId, Long orderId) {
        Optional<User> user = userRepository.findById(userId);
        Optional<Order> order;
        if (user.isPresent())
            order = orderRepository.findByIdAndUser(orderId, user.get());
        else
            throw new UserNotFoundException();

        if (order.isPresent())
            return modelMapper.map(order, OrderDTO.class);
        else throw new OrderNotFoundException();
    }

    @Override
    public boolean checkOrder(Long userId, Long certificateId) {
        Optional<Order> order = orderRepository
                .findOrderByUserIdAndCertificateId(userId, certificateId);

        return order.isPresent();
    }

    @Override
    @Transactional
    public OrderDTO addOrder(Long userId, Long certificateId) {

        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty())
            throw new UserNotFoundException();

        Optional<Certificate> certificate = certificateRepository.findById(certificateId);
        if (certificate.isEmpty())
            throw new CertificateNotFoundException();

        if (checkOrder(userId, certificateId))
            throw new ServiceException(ORDER_EXISTS);

        Order order = new Order().toBuilder()
                .user(user.get())
                .certificate(certificate.get())
                .price(certificate.get().getPrice())
                .purchaseDate(LocalDateTime.now())
                .build();

        orderRepository.save(order);

        return modelMapper.map(order, OrderDTO.class);
    }

}
