package com.epam.esm.service;


import com.epam.esm.service.dto.UserDTO;

import java.util.List;

/**
 * The UserService interface represents a service for managing user entities.
 */
public interface UserService extends Service<UserDTO> {

    /**
     * Retrieves a list of UserDTO objects with pagination support.
     *
     * @param page The page number to retrieve.
     * @param size The number of items per page.
     * @return A list of UserDTO objects.
     */
    List<UserDTO> findWithPagination(Integer page, Integer size);

    /**
     * Retrieves a UserDTO object by login.
     *
     * @param login The login of the user to retrieve.
     * @return The UserDTO object with the specified login, or null if not found.
     */
    UserDTO getByLogin(String login);

}