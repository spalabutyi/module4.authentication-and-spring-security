package com.epam.esm.service.dto;

import com.epam.esm.model.Role;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.stereotype.Component;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "email"}, callSuper = false)
@ToString
@Component
public class UserDTO extends AbstractDTO {

    @NotBlank
    @Size(min = 1, max = 32, message = "Name must be between 1 and 32 characters")
    private String name;

    @Email
    private String email;

    @NotBlank
    @Size(min = 8, max = 32, message = "Must be between 8 and 32 char")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonIgnore
    private Role role;

    @JsonBackReference
    private Set<OrderDTO> orders;

    public UserDTO(String name, String email) {
        this.name = name;
        this.email = email;
    }
}