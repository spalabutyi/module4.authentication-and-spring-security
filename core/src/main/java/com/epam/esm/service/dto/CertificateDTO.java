package com.epam.esm.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.*;
import lombok.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"name", "description", "price", "duration", "createDate", "lastUpdateDate"}, callSuper = false)
@Component
public class CertificateDTO extends AbstractDTO {

    @NotBlank(message = "Name is required")
    @Size(min = 1, max = 32, message = "Name must be between 1 and 32 characters")
    private String name;

    @NotBlank(message = "Description is required")
    @Size(min = 1, max = 256, message = "Description must be between 1 and 256 characters")
    private String description;

    @NotNull(message = "Price is required")
    @Positive
    @DecimalMin(value = "0.01", message = "Price must be greater than or equal to 0.01")
    @Digits(integer = 9, fraction = 2, message = "Patter: xxxxxxxxx.yy")
    private BigDecimal price;

    @NotNull(message = "Duration is required")
    @Min(value = 1, message = "Duration must be at least 1 day")
    @Max(value = 365, message = "Duration cannot be more than 365 days")
    private Integer duration;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime createDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime lastUpdateDate;

    private Set<TagDTO> tags;

    public CertificateDTO(String name, String description, BigDecimal price, Integer duration) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.duration = duration;
    }
}