package com.epam.esm.repository.exception;

public class UserFailCreateException extends RuntimeException{

    public UserFailCreateException(String message) {
        super(message);
    }
}
