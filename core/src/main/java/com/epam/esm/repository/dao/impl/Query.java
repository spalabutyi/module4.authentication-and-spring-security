package com.epam.esm.repository.dao.impl;

public abstract class Query {

    public static final String GET_WIDELY_USED_TAG = "SELECT tag.id " +
            "FROM tag " +
            "INNER JOIN gift_certificate_tag ON tag.id = gift_certificate_tag.tag_id " +
            "INNER JOIN (SELECT certificate_id, MAX(orders.price) as max_price " +
            "FROM gift_certificate " +
            "INNER JOIN orders ON orders.certificate_id = gift_certificate.id " +
            "GROUP BY certificate_id) AS gc_max_price " +
            "ON gc_max_price.certificate_id = gift_certificate_tag.gift_certificate_id " +
            "INNER JOIN orders ON orders.certificate_id = gc_max_price.certificate_id " +
            "WHERE orders.user_id = ? " +
            "GROUP BY tag.id " +
            "ORDER BY COUNT(*) DESC " +
            "LIMIT 1 ";

    private Query() {
    }
}
