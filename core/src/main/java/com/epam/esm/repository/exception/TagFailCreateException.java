package com.epam.esm.repository.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TagFailCreateException extends RuntimeException{

    public TagFailCreateException(String message) {
        super(message);
    }
}
