package com.epam.esm.repository.exception;

public class CertificateFailUpdateException extends RuntimeException{

    public CertificateFailUpdateException(String message) {
        super(message);
    }

}
