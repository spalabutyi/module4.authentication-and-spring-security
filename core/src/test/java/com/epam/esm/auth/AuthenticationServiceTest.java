package com.epam.esm.auth;

import com.epam.esm.model.User;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.repository.exception.UserFailCreateException;
import com.epam.esm.repository.exception.UserNotFoundException;
import com.epam.esm.security.JwtUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import static com.epam.esm.model.Role.USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@Profile("test")
class AuthenticationServiceTest {

    @InjectMocks
    private AuthenticationService service;
    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtUtil jwtUtil;
    @Mock
    private AuthenticationManager manager;

    @BeforeEach
    void setUp() {
        service = new AuthenticationService(
                userRepository,
                passwordEncoder,
                jwtUtil,
                manager
        );
    }

    @Test
    @Tag("register")
    void newUserRegisterSuccessTest() {
        String name = "User 1";
        String email = "user1@mail.com";
        String password = "password";
        String passwordEncoded = "wordpass";
        String jwtToken = "kls456jesef4sd24fs54df4sdf4sd68f46s4df";
        String refreshJwtToken = "4sd24fs54df4sdf4sd68f45df4df5ds4d56s4df";
        Instant now = Instant.now();

        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setName(name);
        registerRequest.setEmail(email);
        registerRequest.setPassword(password);

        User user = User.builder()
                .name(name)
                .email(email)
                .password(passwordEncoded)
                .role(USER)
                .build();
        when(userRepository.findByEmail(email)).thenReturn(Optional.empty());
        when(passwordEncoder.encode(anyString())).thenReturn(passwordEncoded);
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(jwtUtil.getToken(any(User.class))).thenReturn(jwtToken);
        when(jwtUtil.getRefreshToken(any(User.class))).thenReturn(refreshJwtToken);
        when(jwtUtil.extractIssue(anyString())).thenReturn(Date.from(now));
        when(jwtUtil.extractExpiration(anyString())).thenReturn(Date.from(now.plusSeconds(120000)));

        AuthResponse response = service.register(registerRequest);

        verify(userRepository).findByEmail(email);
        verify(userRepository).save(user);
        verify(jwtUtil).getToken(user);
        verify(jwtUtil).getRefreshToken(user);

        assertEquals(response.getToken(), jwtToken);
        assertEquals(response.getRefreshToken(), refreshJwtToken);
        assertTrue(response.getIssuedAt().before(response.getExpiresAt()));

    }

    @Test
    @Tag("register")
    void newUserRegisterFailTest() {
        String name = "User 1";
        String email = "user1@mail.com";
        String password = "password";
        String passwordEncoded = "wordpass";

        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setName(name);
        registerRequest.setEmail(email);
        registerRequest.setPassword(password);

        User user = User.builder()
                .name(name)
                .email(email)
                .password(passwordEncoded)
                .role(USER)
                .build();
        when(userRepository.findByEmail(email)).thenReturn(Optional.of(user));
        assertThrows(UserFailCreateException.class, () ->
                service.register(registerRequest));

    }

    @Test
    @Tag("authenticate")
    void userAuthenticateSuccessTest() {
        String name = "User 1";
        String email = "user1@mail.com";
        String password = "password";

        AuthRequest authRequest = new AuthRequest();
        authRequest.setEmail(email);
        authRequest.setPassword(password);

        User user = User.builder()
                .name(name)
                .email(email)
                .password(password)
                .role(USER)
                .build();
        user.setId(5L);

        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null);

        when(manager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenReturn(authentication);
        when(userRepository.findByEmail(email)).thenReturn(Optional.of(user));

        AuthResponse response = service.authenticate(authRequest);

        assertEquals(response.getUserId(), user.getId());

    }

}